#!/bin/bash

set -ex

umask 077

cores="$(nproc)"

if [ "${cores}" -gt 1 ]; then
    jobs="$(( cores / 2 ))"
else
    jobs='1'
fi

makejobs="${cores}"
emerge_extra_opts="-j${jobs} --backtrack=500"
export XZ_OPT="--threads=${cores}"

timezone='Europe/Warsaw'

basic_apps="vixie-cron metalog portage-utils genlop gentoolkit ash-bb zsh gentoo-zsh-completions vim elogv mdev-like-a-boss iproute2 busybox"
extra_apps="pwgen colordiff strace gdb bind-tools whois mlocate pciutils ncdu htop iotop atop lsof links ccze logrotate iftop nettop nethogs netcat telnet-bsd socat acpid chrony unscd"

install_apps="${basic_apps} ${extra_apps}"

# To disable buildpkg during this script run, but keep it in the make.conf.
export FEATURES='-buildpkg'

### ### ###

has() {
    # Return true if $1 is present in any of $*.
    local check_for
    check_for="$1"
    shift
    for i in "$@"; do
        if [ "${check_for}" = "${i}" ]; then
            return 0
        fi
    done
    return 1
}

bump_configs() {
	find "/etc" -name '._cfg*' | while read line; do
		targetfile="$(echo "${line}" | sed -r 's|\._cfg[[:digit:]]+_||')"
		sourcefile="${line}"

		if ! has "${targetfile}" "${protected_configs[@]}"; then
			echo "[bump_configs] Replacing ${targetfile/\/etc\//}"
			mv "${sourcefile}" "${targetfile}"
		else
			echo "[bump_config] Not replacing ${targetfile/\/etc\//}"
			rm "${sourcefile}"
		fi
	done
}


set_package_keyword() {
	# $1 file name.
	# $2 package name
	# $3 keyword
	if ! [ -d /etc/portage/package.keywords ]; then
		mkdir /etc/portage/package.keywords
	fi

	if [ "$#" != '3' ]; then
		echo 'Wrong number of params to set_package_keyword'
		exit 1
	fi

	echo "$2 $3" >>"/etc/portage/package.keywords/$1"
}

set_package_use() {
	# $1 file name.
	# $2 package name
	# $3 flags
	if ! [ -d /etc/portage/package.use ]; then
		mkdir /etc/portage/package.use
	fi

	if [ "$#" != '3' ]; then
		echo 'Wrong number of params to set_package_use'
		exit 1
	fi

	echo "$2 $3" >>"/etc/portage/package.use/$1"
}

set_package_mask() {
	# $1 file name.
	# $2 package name
	# $3 flags
	if ! [ -d /etc/portage/package.mask ]; then
		mkdir /etc/portage/package.mask
	fi

	if [ "$#" != '2' ]; then
		echo 'Wrong number of params to set_package_use'
		exit 1
	fi

	echo "$2" >>"/etc/portage/package.mask/$1"
}

set_package_provided() {
	if ! [ -d /etc/portage/profile ]; then
		mkdir /etc/portage/profile
	fi

	if [ "$#" != '1' ]; then
		echo 'Wrong number of params to set_package_use'
		exit 1
	fi

	echo "$1" >>"/etc/portage/profile/package.provided"
}

if ! mountpoint -q /dev/shm; then
    echo 'Remember to mount /dev/shm with mode 1777'
    echo 'But first check if thats not a symlink!'
    exit 1
fi

while [ "$#" -ge '1' ]; do
	case "$1" in
		--arch)
			case "$2" in
				'amd64')
					readonly generic_arch='x86_64'
					#readonly march='native'
					readonly keyword='amd64'
				;;
			    *)
				    echo "Wrong --arch."
				    exit 1
			    ;;
			esac
			shift 2
		;;
		*)
			echo "Wrong args"
			exit 1
		;;
	esac
done


readonly base_cflags="-O${O_level:-2} -pipe -mtune=generic"
if [ "${march}" ]; then
	readonly cflags="${base_cflags} -march=${march}"
else
	readonly cflags="${base_cflags}"
fi

### make.conf
cat > /etc/portage/make.conf <<EOF
CFLAGS="${cflags}"
CXXFLAGS="\${CFLAGS}"
CHOST="${generic_arch}-pc-linux-gnu"
ACCEPT_KEYWORDS="~${keyword}"

PORTAGE_NICENESS="19"
L10N="en"

FEATURES="parallel-fetch -config-protect-if-modified noinfo buildpkg userpriv -sandbox -usersandbox"

MAKEOPTS="--quiet -j${makejobs}"

# mirrors
GENTOO_MIRRORS="http://ftp.heanet.ie/pub/gentoo/ http://distfiles.gentoo.org/"

CPU_FLAGS_X86="mmx mmxext sse sse2"

USE="
bindist
pax_kernel pic
threads smp
xft fontconfig opengl lcdfilter infinality -cleartype
logrotate zsh-completion cups vim-syntax jpeg png
-networkmanager -arts -gstreamer -hal -pm-utils -introspection -udev -X -udisks -ipv6 -pulseaudio
"

ACCEPT_LICENSE="*"

PORTAGE_COMPRESS = "bzip2"
PORTAGE_COMPRESS_FLAGS = "-9"

# Save elog, viewable by elogv.
PORTAGE_ELOG_SYSTEM="save" 
PORTAGE_ELOG_CLASSES="warn error info log qa"

# Utilize /var/portage.
PORTAGE_HOME="/var/portage"
PORTAGE_TMPDIR="\${PORTAGE_HOME}/tmp"
PORTDIR="\${PORTAGE_HOME}/tree"
DISTDIR="\${PORTAGE_HOME}/distfiles"
PKGDIR="\${PORTAGE_HOME}/packages"

# Some better defaults ...
EMERGE_DEFAULT_OPTS="
--with-bdeps y
--binpkg-respect-use y
--buildpkg-exclude 'virtual/* sys-kernel/*-sources games-*/* app-emulation/emul-linux-*'
"

# Real multiarch.
ABI_X86="64 32"
EOF
protected_configs+=( '/etc/portage/make.conf' )

### Create dir structure for portage.
mkdir -p "/var/portage"/{tree,tmp,packages,distfiles}
ln -s ../var/portage/tree /usr/portage
chown -R portage:portage /var/portage
chmod 750 /var/portage

### Unpack any portage-latest tarball.
for i in /portage-latest.tar*; do
    tar -xf "${i}" -C /var/portage/tree --strip-components 1
    break
done

# Use flags
set_package_use system 'sys-apps/busybox' 'static'
set_package_use system 'sys-fs/eudev' '-* abi_x86_32'

# hwids need udev useflag to install /etc/udev/hwdb.bin, which is needed by lsusb
# to display device names.
set_package_use system 'sys-apps/hwids' 'udev'

set_package_use system 'app-portage/layman' 'git'
set_package_use general 'app-editors/vim' 'vim-pager'
set_package_use x_and_media 'x11-*/*' 'X'
set_package_use x_and_media 'media-*/*' 'X truetype alsa vorbix g3dvl x264 xvid vdpau mp3 vorbis flac' 

# Make glibc utilize (u)nscd.
set_package_use system 'sys-libs/glibc' 'nscd'

# Mask gnupg-2 which is major pain in the ass.
set_package_mask 'drop_gnupg-2' '=app-crypt/gnupg-2*'

# Mask new gperf as things fails to build with it.
set_package_mask 'shiny_new_gperf' '=dev-util/gperf-3.1'

# Broken busybox
set_package_mask 'busybox' '=sys-apps/busybox-1.26.2'

# Get updated portage first
emerge -u portage ${emerge_extra_opts}

# Get git upfront.
USE='-perl -python -webdav' emerge dev-vcs/git ${emerge_extra_opts} --noreplace

# Deploy localpatch's patches.
( cd /etc/portage && git clone 'https://github.com/slashbeast/localpatch-patches.git' localpatches )
cat >/etc/portage/postsync.d/localpatch-patches-sync <<EOF
#!/bin/sh
umask 022
if [ -d '/etc/portage/localpatches' ] && ! [ -h '/etc/portage/localpatches' ]; then
	( cd '/etc/portage/localpatches' && git pull --rebase )
fi
EOF
chmod +x /etc/portage/postsync.d/localpatch-patches-sync

# Run layman sync after emerge --sync, if installed.
cat >/etc/portage/postsync.d/layman-sync <<EOF
#!/bin/sh
umask 022
if [ -f '/usr/bin/layman' ]; then
	/usr/bin/layman -S
fi
EOF
chmod +x /etc/portage/postsync.d/layman-sync

# Config portage, some keywords, useflags.
chmod 750 /etc/portage
chown portage:portage -R /etc/portage

### Timezone.
cp "/usr/share/zoneinfo/${timezone}" /etc/localtime
echo "${timezone}" >/etc/timezone
chmod 644 /etc/timezone /etc/localtime
protected_configs+=( '/etc/localtime' '/etc/timezone' )

### Locales
cat >/etc/locale.gen <<EOF
en_US ISO-8859-1
en_US.UTF-8 UTF-8
pl_PL ISO-8859-2
pl_PL.UTF-8 UTF-8
EOF
protected_configs+=( '/etc/locale.gen' )
locale-gen
echo 'export LC_ALL="en_US.UTF-8"' >/etc/env.d/02locale

# Set profile, first should be the most basic one.
eselect profile list | sed '/\[1\]/!d'
eselect profile set 1

env-update

# Buggy layman.
set_package_mask 'buggy_layman'  '=app-portage/layman-2.3.0*'

### Get layman and add foo-overlay.
emerge layman ${emerge_extra_opts} --noreplace
layman -L >/dev/null 2>&1
sed 's@check_official : Yes@check_official : No@g' -i /etc/layman/layman.cfg
layman -a foo-overlay

### Add Foobashrc and enable localpatch and striplafiles features.
emerge foobashrc ${emerge_extra_opts} --noreplace
( cd /etc/portage && ln -s foobashrc.bashrc bashrc )
echo 'foobashrc_modules="localpatch striplafiles break_hardlinks"' >>/etc/portage/make.conf

# Get eudev as udev stub, Mesa and some other things require it as build time dep
# and as we disable all useflags for it, it won't hurt to have it around.
# That will provide cover the needs for virtual/dev-manager -> virtual/udev.
emerge -C udev
emerge -C sys-fs/udev-init-scripts
set_package_provided 'sys-fs/udev-init-scripts-9999'
emerge eudev ${emerge_extra_opts} --noreplace
emerge virtual/udev ${emerge_extra_opts}

## Bump perl first, so we won't crash on help2man and such.
#emerge perl --nodeps -1
#perl-cleaner --all -- -j${jobs}
emerge -NuD perl ${emerge_extra_opts}

# Update gcc and friends first.
# And switch to the latest one.
emerge -NuD -1 --noreplace ${emerge_extra_opts} gcc glibc binutils
gcc-config $(gcc-config -C -l | perl -e 'while(<>) { next unless eof; s/^ *\[([0-9]+)\].*/\1/g; print }')
env-update

emerge -NuD @system ${emerge_extra_opts}
emerge -NuD @world ${emerge_extra_opts}
perl-cleaner --all -- -j${jobs} ${emerge_extra_opts}

### Bump config files.
bump_configs

### Rebuild openrc (localpatches) and update scripts.
emerge -1 openrc
bump_configs

### Cleanup leftover packages.
emerge --depclean

### Fix preserve libs, if any
emerge @preserved-rebuild ${emerge_extra_opts}

### Install additional apps.
emerge ${emerge_extra_opts} --noreplace ${install_apps}

### Again, bump config files, if needed.
bump_configs

### Enable mdev, drop devfs and udev* as mdev init script will do all the things.
rc-update add mdev sysinit
for i in /etc/runlevels/*/; do
    [ "${i}" = '/etc/runlevels/*/' ] && break
    rm -f "${i}"/devfs "${i}"/udev*
done

### Add metalog and vixie cron to runlevels.
rc-update add vixie-cron default
rc-update add metalog boot

### Deploy ash as /bin/sh
test -f /bin/ash && ln -snf ash /bin/sh && echo '/bin/ash' >>/etc/shells
protected_configs+=( '/etc/shells' )

### Consolefont.
sed -r 's:^consolefont=\".+\"$:consolefont=\"lat2-16\":' /etc/conf.d/consolefont -i
rc-update add consolefont boot
protected_configs+=( '/etc/conf,d/consolefont' )

### chrony, the ntp client.
rc-update add chronyd default

### unscd
rc-update add unscd default

### Mark news items as red.
eselect news read new > /dev/null 2>&1

### Config dispatch-conf to use colordiff inetead of diff.
sed -r "s:^diff=\".+\"$:diff=\"colordiff -Nu '%s' '%s' | less --no-init --QUIT-AT-EOF -R\":" /etc/dispatch-conf.conf -i
protected_configs+=( '/etc/dispatch-conf.conf' )

### Adjust colordiff config to something more readable.
cat >/etc/colordiffrc <<EOF
banner=no
color_patches=no
diff_cmd=diff
plain=off
newtext=green
oldtext=red
diffstuff=darkcyan
cvsstuff=cyan
EOF
protected_configs+=( '/etc/colordiffrc' )

### Append '--noclear' option to agetty to prevent clearing the screen on boot.
sed 's#\(respawn:/sbin/agetty\)\ \([0-9].*\)#\1 --noclear \2#g' /etc/inittab -i
protected_configs+=( '/etc/inittab' )

### Make `mount` print real state of mounts.
ln -snf ../proc/mounts /etc/mtab

### Adjust rc.conf.
cp /etc/rc.conf /etc/rc.conf.orig
cat >/etc/rc.conf <<EOF
rc_shell=/sbin/sulogin
unicode="YES"

# Don't restart any service when we restart network interfaces.
rc_provide="!net"
rc_loopback_provide='net'

rc_tty_number=12

# Disable cgroups automounting in openrc.
rc_controller_cgroups=NO

# Save log
rc_logger="yes"
EOF
protected_configs+=( '/etc/rc.conf' )

### Prefer IPv4 over IPv6
echo 'precedence ::ffff:0:0/96  100' >>/etc/gai.conf
protected_configs+=( '/etc/gai.conf' )

### Flush often, flush small!
cat >>/etc/sysctl.conf <<EOF

# Force flush when dirty reach 512M
vm.dirty_bytes=536870912
# Start non-blocking flush when dirty reaches 50M
vm.dirty_background_bytes=52428800
EOF

### Don't purge VFS cache too much.
cat >>/etc/sysctl.conf <<EOF

# Drop VFS caches less often.
vm.vfs_cache_pressure=50
EOF

# Reduce amount of memory kept for buffers.
cat >>/etc/sysctl.conf <<EOF

# Reduce swappiness to 30% of system memory.
vm.swappiness=30
EOF
protected_configs+=( '/etc/sysctl.conf' )

### Utilize per-user tmp dir.
cat >/etc/profile.d/home-tmp.sh <<'EOF'
export TMPDIR="$HOME/tmp"
export TMP="$TMPDIR"
EOF
chmod 755 /etc/profile.d/home-tmp.sh
mkdir /etc/skel/tmp

### Set default umask of 077 for login shells.
sed -r 's@umask [0-9]+@umask 077@g' -i /etc/profile
protected_configs+=( '/etc/profile' )

### Set max number of processes an user can have. 
### Better to have very large amount than unlimited.
echo '*   hard   nproc   4096' >>/etc/security/limits.conf
protected_configs+=( '/etc/security/limits.conf' )

# CVE 2004-2778, Gentoo Bug #607426.
chown -c root:cron /var/spool/cron
chmod -c 750 /var/spool/cron

### Remove bindist flag.
sed -r '/^bindist$/d' -i /etc/portage/make.conf

# Just in case.
bump_configs
