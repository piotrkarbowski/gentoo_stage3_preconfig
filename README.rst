gentoo_stage3_preconfig
=======================

Why?
----

One of the most annoying thing I am facing while doing new gentoo deployment is the needs of updating whole system to ~arch first and resolving some blockers, then bumping all perl and python modules and doing some basic configuration like make.conf, sysctl.conf, nuking udev etc. Most of the steps are suitable for every single deployment I am doing thus I've decided to write a script that will do it all for me meaning to have minimal bootable  system where I can freely add software I just need to run given script, add kernel, boot laoder and boot it all up.

Random usage notes
------------------

- Building Mesa: One may need to drop evdev from package.provided and emerge it with 'sys-fs/evdev -*' as libudev is build time dependency for mesa now.
- Useflag infinality and libXft: there are circular dependencies around infinality on freetype needs libXft to be already installed. Solution would be building libXft and its deps without infinality flag and then enabling it.
